//グローバル変数（アプリ側から渡される情報）
float4x4 WVP;	//ワールド、ビュー、プロジェクション行列を合成したもの



//【頂点シェーダー】
//この関数は、描画する物体の頂点の数だけ1フレームに呼ばれる
//基本的な仕事は頂点のローカル座標をスクリーン座標まで変換すること
//その他にも頂点単位で行いたい処理があればここに書くことになる
//
//「POSITION」や「SV_POSITION」は『セマンティクス』といい
//その変数が何の情報を扱うのかを表す
//
//引数：pos　ローカル座標での頂点位置
//戻値：スクリーン座標での頂点位置
float4 VS(float4 pos : POSITION) : SV_POSITION
{
	/*
	//こうしたらどうなるだろう
	pos.x += 1;
	*/

	//引数で受け取ったローカル座標に、グローバルで受け取った行列を掛ける
	//これでローカル座標はスクリーン座標に変換される
	float4 outPos = mul(pos, WVP);

	/*
	//こうしたらどうなるだろう
	outPos.x += 1;
	*/

	//変換されたスクリーン座標を出力する
	return outPos;
}




//【ピクセルシェーダー】
//頂点シェーダー実行後、ラスタライズすることで
//ディスプレイのどのピクセルを光らせればいいのかが分かる。
//その後、光らせるべき頂点ごとにこの関数が呼ばれる。
//
//この関数の仕事は、そのピクセルを何色にするかを求めること。
//基本的にはテクスチャの該当する位置の色になるが、
//影やハイライトによって変化する
//
//引数：pos　該当するピクセルのスクリーン座標での位置
//戻値：そこを何色にするか
float4 PS(float4 pos : SV_POSITION) : COLOR
{
	//とりあえず、無条件で赤にしてみる
	float4 color = float4(1, 0, 0, 1);

	/*
	//こうしたらどうなるだろう
	color.g = pos.x / 1280;
	*/

	/*
	//こうしたらどうなるだろう
	if (pos.x > 640)
	{
		color.a = 0;
	}
	*/

	//その色に決定
	return color;
}



//【テクニック】
//ここで、どの関数が頂点シェーダーで、
//どの関数がピクセルシェーダーなのかを指定する
technique
{
	//頂点シェーダーとピクセルシェーダーのセットは複数作ることができる
	//そのセットのことをパスと表現する
	pass
	{
		//VSという関数をバージョン3でコンパイルしたものを頂点シェーダーとする
		VertexShader = compile vs_3_0 VS();	

		//PSという関数をバージョン3でコンパイルしたものをピクセルシェーダーとする
		PixelShader = compile ps_3_0 PS();
	}
}