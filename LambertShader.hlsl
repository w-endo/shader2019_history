//グローバル変数（アプリ側から渡される情報）
float4x4 WVP;	//ワールド、ビュー、プロジェクション行列を合成したもの
float4x4 W;		//ワールド行列（法線回す用）
float4	 LIGHT_DIR;
float4	 DIFFUSE_COLOR;


//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos   :SV_POSITION;	//位置
	float4 color :COLOR;		//色
};



//【頂点シェーダー】
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL)
{
	//仮のライト
	float4 lightDir = -LIGHT_DIR;	//左上手前
	lightDir = normalize(lightDir);			//向きだけが必要なので正規化

	//出力データ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);			//位置（今まで通り）

	normal = mul(normal, W);				//オブジェクトが変形すれば法線も変形
	normal = normalize(normal);			//法線を正規化

	outData.color = dot(normal, lightDir);	//色というか影（法線とライトベクトルの内積）
	outData.color.a = 1;					//暗い部分はαまで小さくなって透けちゃうので、強制的に不透明にする
	
	outData.color *= DIFFUSE_COLOR;

	outData.color += float4(0.2, 0.2, 0.2, 0);

	//まとめて出力
	return outData;
}




//【ピクセルシェーダー】
float4 PS(VS_OUT inData) : COLOR
{
	//頂点シェーダーで色を求めてるので、そのまま出力
	return inData.color;
}



//【テクニック】
technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}