//グローバル変数（アプリ側から渡される情報）
float4x4 WVP;	//ワールド、ビュー、プロジェクション行列を合成したもの
float4x4 RS;	//回転行列と拡大の逆行列
float4x4 W;		//ワールド行列
float4	 LIGHT_DIR;
float4	 DIFFUSE_COLOR;
float4	 CAMERA_POS;	//視点（カメラの位置）
texture	 TEXTURE;


//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
};


//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float4 normal : NORMAL;			//法線
	float4 eye	  : TEXCOORD1;		//視線
	float2 uv	  : TEXCOORD0;		//UV座標
};



//【頂点シェーダー】
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);			//位置（今まで通り）

	normal = mul(normal, RS);				//オブジェクトが変形すれば法線も変形
	normal = normalize(normal);			//法線を正規化
	outData.normal = normal;

	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	outData.uv = uv;


	//まとめて出力
	return outData;
}




//【ピクセルシェーダー】
float4 PS(VS_OUT inData) : COLOR
{
	float4 lightDir = LIGHT_DIR;	//左上手前
	lightDir = normalize(lightDir);	//向きだけが必要なので正規化

	//拡散反射光
	float4 diffuse = dot(inData.normal, -lightDir);
	diffuse.a = 1;
	//diffuse *= DIFFUSE_COLOR;
	diffuse *= tex2D(texSampler, inData.uv);

	//環境光
	float4 ambient = float4(0.2, 0.2, 0.2, 0);

	//鏡面反射光
	float speSize = 40.0f;
	float spePower = 2.0f;
	float4 R = reflect(lightDir, inData.normal);	//正反射ベクトル
	float4 speculer = pow(dot(R, inData.eye), speSize) * spePower;


	//頂点シェーダーで色を求めてるので、そのまま出力
	return ambient + diffuse + speculer;
}



//【テクニック】
technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}