#include "camera.h"
#include "../DirectX/Direct3D.h"


//コンストラクタ
Camera::Camera(IGameObject* parent)
	: IGameObject(parent, "Camera"), _target(D3DXVECTOR3(0, 0, 0))
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化（プロジェクション行列作成）
void Camera::Initialize()
{
	//プロジェクション行列
	D3DXMATRIX proj;
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), Direct3D::aspect_, 1.0f, 100.0f);
	Direct3D::pDevice_->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新（ビュー行列作成）
void Camera::Update()
{
	D3DXVECTOR3 worldPos;
	D3DXVec3TransformCoord(&worldPos, &_position, &worldMatrix_);

	D3DXVECTOR3 worldTarget;
	D3DXVec3TransformCoord(&worldTarget, &_target, &worldMatrix_);

	

	//ビュー行列
	D3DXMATRIX view;
	D3DXMatrixLookAtLH(&view, &worldPos, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &view);
}

