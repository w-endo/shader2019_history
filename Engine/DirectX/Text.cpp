#include "Text.h"

//コンストラクタ
Text::Text(std::string fontName, int size) :_pFont(nullptr), _color(D3DCOLOR_ARGB(255, 255, 255, 255))
{
	D3DXCreateFont(
		Direct3D::pDevice_,			//デバイスオブジェクト
		size,						//文字サイズ（高さ）
		0,							//文字サイズ（幅）
		FW_NORMAL,					//太さ
		1,							//MIPMAPのレベル
		FALSE,						//斜体
		DEFAULT_CHARSET,			//文字セット
		OUT_DEFAULT_PRECIS,			//出力精度
		DEFAULT_QUALITY,			//出力品質
		DEFAULT_PITCH | FF_SWISS,	//ピッチとファミリ
		fontName.c_str(),			//フォント名
		&_pFont);					//フォントオブジェクト

}

//デストラクタ
Text::~Text()
{
	SAFE_RELEASE(_pFont);
}

void Text::SetColor(DWORD r, DWORD g, DWORD b, DWORD alpha)
{
	_color = D3DCOLOR_RGBA(r, g, b, alpha);
}


void Text::Draw(int x, int y, std::string text)
{
	RECT rect = { x, y, 0, 0 };
	_pFont->DrawText(NULL, text.c_str(), -1, &rect, DT_LEFT | DT_NOCLIP, _color);
}
