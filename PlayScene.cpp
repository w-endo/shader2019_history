﻿#include "PlayScene.h"
#include "Torus.h"
#include "Water.h"
#include "Screen.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	CreateGameObject<Torus>(this);
	CreateGameObject<Water>(this);
	CreateGameObject<Screen>(this);


}

//更新
void PlayScene::Update()
{

}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}