//グローバル変数（アプリ側から渡される情報）
float4x4 WVP;	//ワールド、ビュー、プロジェクション行列を合成したもの
float4x4 RS;	//回転行列と拡大の逆行列
float4x4 W;		//ワールド行列
float4	 LIGHT_DIR;
float4	 DIFFUSE_COLOR;
float4	 AMBIENT_COLOR;
float4	 SPECULER_COLOR;
float	 SPECULER_POWER;
float4	 CAMERA_POS;	//視点（カメラの位置）
bool	 IS_TEXTURE;
texture	 TEXTURE;
texture	 TEXTURE_TOON;

//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler toonSampler = sampler_state
{
	Texture = <TEXTURE_TOON>;
	AddressU = Clamp;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};


//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float4 normal : NORMAL;			//法線
	float4 eye	  : TEXCOORD1;		//視線
	float2 uv	  : TEXCOORD0;		//UV座標
};



//【頂点シェーダー】
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);			//位置（今まで通り）

	normal = mul(normal, RS);				//オブジェクトが変形すれば法線も変形
	normal = normalize(normal);			//法線を正規化
	outData.normal = normal;

	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	outData.uv = uv;


	//まとめて出力
	return outData;
}




//【ピクセルシェーダー】
float4 PS(VS_OUT inData) : COLOR
{
	inData.normal = normalize(inData.normal);
	inData.eye = normalize(inData.eye);

	float4 lightDir = LIGHT_DIR;	//左上手前
	lightDir = normalize(lightDir);	//向きだけが必要なので正規化

	//拡散反射光
	float u = dot(inData.normal, -lightDir);
	float4 diffuse = tex2D(toonSampler, float2(u, 0));
	diffuse.a = 1;

	if (IS_TEXTURE == false)
	{
		diffuse *= DIFFUSE_COLOR;
	}
	else
	{
		diffuse *= tex2D(texSampler, inData.uv);
	}

	//環境光
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射光
	float4 R = reflect(lightDir, inData.normal);	//正反射ベクトル
	float4 speculer = pow(dot(R, inData.eye), SPECULER_POWER) * 2 * SPECULER_COLOR;


	//頂点シェーダーで色を求めてるので、そのまま出力
	//return  ambient + diffuse + speculer;
	return diffuse;
}


float4 VS_Toon(float4 pos : POSITION, float4 normal : NORMAL) : SV_POSITION
{
	normal.w = 0;
	pos += normal / 20;
	pos = mul(pos, WVP);
	return pos;
}

float4 PS_Toon(float4 pos : SV_POSITION) : COLOR
{
	return float4(0,0,0,1);
}



//【テクニック】
technique
{
	//通常描画
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}

	//輪郭
	pass
	{
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader = compile ps_3_0 PS_Toon();
	}
}