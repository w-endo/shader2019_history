#include "Water.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Water::Water(IGameObject * parent)
	:IGameObject(parent, "Water"), hModel_(-1), pEffect_(nullptr), pToonTex_(nullptr)
{
}

//デストラクタ
Water::~Water()
{
	SAFE_RELEASE(pToonTex_);
	SAFE_RELEASE(pEffect_);
}

//初期化
void Water::Initialize()
{

	//HLSLファイルからシェーダーを作成
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
		"WaterShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
		&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}


	//モデルデータのロード
	hModel_ = Model::Load("data/Water.fbx", pEffect_);
	assert(hModel_ >= 0);

	//アニメ風シェード用画像
	D3DXCreateTextureFromFileEx(Direct3D::pDevice_, "data\\toon.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pToonTex_);

}

//更新
void Water::Update()
{

	//回転
	//rotate_.y += 1;
}

//描画
void Water::Draw()
{
	//ワールド行列をセット
	//※これは固定機能パイプラインに必要なもの
	//　シェーダーを使って描画する場合は必要ない
	Model::SetMatrix(hModel_, worldMatrix_);

	//ビュー行列を取得
	//SetTransformでセットした行列はGetTransformでゲットできる
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);

	//プロジェクション行列取得
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

	//ワールド、ビュー、プロジェクション行列を合成
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//合成した行列をシェーダーに渡す
	//HLSL内のグローバル変数を使ってやり取りする
	//第1引数はグローバル変数名（文字列）
	//第2引数は渡す情報のアドレス
	pEffect_->SetMatrix("WVP", &matWVP);


	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);

	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY;

	pEffect_->SetMatrix("RS", &mat);

	//ライトの向きをシェーダに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice_->GetLight(0, &lightState);
	pEffect_->SetVector("LIGHT_DIR",
		(D3DXVECTOR4*)&lightState.Direction);



	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 2, -5));

	//ワールド行列
	pEffect_->SetMatrix("W", &worldMatrix_);

	pEffect_->SetTexture("TEXTURE_TOON", pToonTex_);

	static float scroll = 0.0f;
	scroll += 0.0004f;
	pEffect_->SetFloat("SCROLL", scroll);

	pEffect_->Begin(NULL, 0);


	pEffect_->BeginPass(0);
	Model::Draw(hModel_);
	pEffect_->EndPass();


	//シェーダーを使った描画終わり。
	//これ以降はDirectXデフォルトの描画に戻る
	pEffect_->End();
}

//開放
void Water::Release()
{
}