texture	 TEXTURE;

//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Mirror;
	AddressV = Mirror;
};

struct VS_OUT
{
	float4 pos    : SV_POSITION;	//位置
	float2 uv	  : TEXCOORD0;		//UV座標
};

VS_OUT VS(float4 pos : POSITION, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;

	outData.pos = pos;
	outData.uv = uv;

	return outData;
}

float4 PS(VS_OUT inData) : COLOR
{
	int power = 10;
	int count = 0;
	float4 color = float4(0,0,0,0);
	for (int x = -power; x <= power; x++)
	{
		for (int y = -power; y <= power; y++)
		{
			color += tex2D(texSampler, 
				float2(inData.uv.x + (1.0 / 1280) * x, 
								inData.uv.y + (1.0 / 720) * y));
			count++;
		}

	}
	color /= count;
	
	return  color;
}

technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}